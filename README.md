# WNIX

WNIX is a UNIX-like Windows Development Environment. It contains one of the
latest versions of  GNU toolchain, must-have GNU command line utilities,
popular scripting languages, Eclipse IDE, and famous version control systems.

## Quick Start Guide

Download the latest version and extract it to any location you wish.  Run
`install.bat` either from command line or by double clicking on it.  When the
install completes, several shortcuts should appear on your Start Menu.  You can
disable vim plugins from the editors or IDEs if you don't like it, but you
would miss a great oppurtunity.

If you need to change your compiler between mingw and msvc run the following
commands:

``` bash
 # Change directory to wnix folder
cd %wnix_home%
# Switch to mingw
mingw
# Switch to msvc
msvc
```

Note that the default is mingw. Once you run the above commands,
you need to restart applications in order to activate proper
environment setup except for the current command line session.

## Creating the Environment from Scratch

You first need some GNU utilities in your PATH. You can download [*full*
Cmder](http://cmder.net/), which contains all the required GNU utilities.  Make
sure you have `curl` in Cmder's PATH variable.

Unfortunately, you should manually
[download](https://www.lfd.uci.edu/~gohlke/pythonlibs) the following wheel
files put them into `wnix/download` directory as we can't get full download
link. See `wnixr.sh` file for details.

- GDAL-3.0.0-cp36-cp36m-win_amd64.whl
- rasterio-1.0.23-cp36-cp36m-win_amd64.whl

Once you have all these utilities, you can run `wnixr.sh` script in Cmder as
shown below. 

`sh wnixr -h` is your friend.

```
sh wnixr.sh -h
Usage: wnixr.sh [options]

Options
   -h, --help           Prints this help message
   -d, --download       Downloads programs
   -x, --extract        Extracts downloaded programs
   -p, --provision      Provisions extracted programs
   -r, --release        Releases a tarball with extracted programs
```

When you want to create a new release, first make your changes then bump
the version number in `wnixr.sh` and finally run the following command.

```
sh wnixr.sh -d -x -p -r
```

If anything goes wrong, delete the files or directories created by the last
operation and issue the above command again. It picks up where it left off
except for provisioning phase. The script creates *.download*, *.extract* and
*.provision* files to indicate the completion of the corresponding phase so
that the next phase is good to go. For example release phase can begin only if
*.provision* file exists.
