@echo off

call %~dp0setenv.bat

if not "%~1"=="vrapper" (
    if not exist .novrapper (
        echo Uninstalling Eclipse Vrapper Plugin...
        %WNIX_HOME%eclipse\eclipsec.exe -application org.eclipse.equinox.p2.director -uninstallIU net.sourceforge.vrapper.feature.group
        type nul >.novrapper
	)
)

echo Creating shortcuts...
call shortcuts.vbs
echo Done!
