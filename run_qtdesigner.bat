@echo off

:: Setup environment variables
call %~dp0setenv.bat

:: Start QtDesigner
start %QTDESIGNER_HOME%\designer.exe
