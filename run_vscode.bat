@echo off

:: Setup environment variables
call %~dp0setenv.bat

:: Start VSCode
start %VSCODE_HOME%\bin\code --extensions-dir %VSCODE_HOME%\extensions
