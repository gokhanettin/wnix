@echo off

:: Development environment root
set WNIX_HOME=%~dp0

:: Java Runtime Environment
set JAVA_HOME=%WNIX_HOME%jre

:: Minimalist GNU for Windows (compiler, linker, etc...)
set MINGW_HOME=%WNIX_HOME%qt5\Tools\mingw530_32

:: Qt5 Libraries
set QT5LIBS_HOME=%WNIX_HOME%qt5\5.9.1\mingw53_32

:: QtCreator
set QTCREATOR_HOME=%WNIX_HOME%qt5\Tools\QtCreator\bin

:: QtDesigner
set QTDESIGNER_HOME=%QT5LIBS_HOME%\bin

:: FreeGLUT
set FREEGLUT_HOME=%WNIX_HOME%freeglut

:: Glew
set GLEW_HOME=%WNIX_HOME%glew

:: Python
set PYTHON_HOME=%WNIX_HOME%python3

:: Cmder Terminal Emulator
set CMDER_HOME=%WNIX_HOME%cmder

:: Perl
set PERL_HOME=%WNIX_HOME%perl

:: Groovy
set GROOVY_HOME=%WNIX_HOME%groovy

:: Svn
set SVN_HOME=%WNIX_HOME%svn

:: CMake
set CMAKE_HOME=%WNIX_HOME%cmake
set CMAKE_PREFIX_PATH=%QT5LIBS_HOME%;%FREEGLUT_HOME%;%GLEW_HOME%

:: Eclipse
set ECLIPSE_HOME=%WNIX_HOME%eclipse

:: VSCode
set VSCODE_HOME=%WNIX_HOME%vscode

:: Extra
set EXTRA_HOME=%WNIX_HOME%extra

:: Prepend PATH
set PATH=%WNIX_HOME%\doxygen:%PATH%
set PATH=%EXTRA_HOME%\bin;%PATH%
set PATH=%VSCODE_HOME%\App\VSCode64\bin;%PATH%
set PATH=%ECLIPSE_HOME%;%PATH%
set PATH=%SVN_HOME%\bin;%PATH%
set PATH=%CMAKE_HOME%\bin;%PATH%
set PATH=%PYTHON_HOME%\Scripts;%PATH%
set PATH=%PYTHON_HOME%;%PATH%
set PATH=%MINGW_HOME%\bin;%PATH%
set PATH=%JAVA_HOME%\bin;%PATH%
set PATH=%FREEGLUT_HOME%\bin;%PATH%
set PATH=%GLEW_HOME%\bin;%PATH%
set PATH=%QT5LIBS_HOME%\bin;%PATH%
