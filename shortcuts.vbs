set oShell = WScript.CreateObject("WScript.Shell" )
strDesktop = oShell.SpecialFolders("Desktop" )
strPrograms = oShell.SpecialFolders("Programs")
strProfile = oShell.ExpandEnvironmentStrings("%USERPROFILE%")
strWnix = oShell.ExpandEnvironmentStrings("%WNIX_HOME%")

' Cmder
strCmder = oShell.ExpandEnvironmentStrings("%CMDER_HOME%")
set oShortcut = oShell.CreateShortcut(strPrograms & "\Cmder.lnk" )
oShortcut.TargetPath = strWnix &  "run_cmder.bat"
oShortcut.WindowStyle = 1
oShortcut.IconLocation = strCmder & "\Cmder.exe"
oShortcut.Description = "Run Cmder"
oShortcut.WorkingDirectory = strProfile
oShortcut.Save

' Eclipse
strEclipse = oShell.ExpandEnvironmentStrings("%ECLIPSE_HOME%")
set oShortcut = oShell.CreateShortcut(strPrograms & "\Eclipse.lnk" )
oShortcut.TargetPath = strWnix &  "run_eclipse.bat"
oShortcut.WindowStyle = 1
oShortcut.IconLocation = strEclipse & "\eclipse.exe"
oShortcut.Description = "Run Eclipse"
oShortcut.WorkingDirectory = strProfile
oShortcut.Save

' QtCreator
strQtCreator = oShell.ExpandEnvironmentStrings("%QTCREATOR_HOME%")
set oShortcut = oShell.CreateShortcut(strPrograms & "\QtCreator.lnk" )
oShortcut.TargetPath = strWnix &  "run_qtcreator.bat"
oShortcut.WindowStyle = 1
oShortcut.IconLocation = strQtCreator & "\qtcreator.exe"
oShortcut.Description = "Run QtCreator"
oShortcut.WorkingDirectory = strProfile
oShortcut.Save

' QtDesigner
strQtDesigner = oShell.ExpandEnvironmentStrings("%QTDESIGNER_HOME%")
set oShortcut = oShell.CreateShortcut(strPrograms & "\QtDesigner.lnk" )
oShortcut.TargetPath = strWnix &  "run_qtdesigner.bat"
oShortcut.WindowStyle = 1
oShortcut.IconLocation = strQtDesigner & "\designer.exe"
oShortcut.Description = "Run QtDesigner"
oShortcut.WorkingDirectory = strProfile
oShortcut.Save

' VSCode
strVSCode = oShell.ExpandEnvironmentStrings("%VSCODE_HOME%")
set oShortcut = oShell.CreateShortcut(strPrograms & "\VSCode.lnk" )
oShortcut.TargetPath = strWnix &  "run_vscode.bat"
oShortcut.WindowStyle = 1
oShortcut.IconLocation = strVSCode & "\VSCodePortable.exe"
oShortcut.Description = "Run VSCode"
oShortcut.WorkingDirectory = strProfile
oShortcut.Save

' Thunderbird
set oShortcut = oShell.CreateShortcut(strPrograms & "\Thunderbird.lnk" )
oShortcut.TargetPath = strWnix & "thunderbird" & "/thunderbird.exe"
oShortcut.WindowStyle = 1
oShortcut.Description = "Run Thunderbird"
oShortcut.WorkingDirectory = strProfile
oShortcut.Save

' Firefox
set oShortcut = oShell.CreateShortcut(strPrograms & "\Firefox.lnk" )
oShortcut.TargetPath = strWnix & "firefox" & "/firefox.exe"
oShortcut.WindowStyle = 1
oShortcut.Description = "Run Firefox"
oShortcut.WorkingDirectory = strProfile
oShortcut.Save

