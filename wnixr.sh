#!/bin/sh

set -e

wnix_version="v0.1.13"

jdk_link=https://download.java.net/openjdk/jdk11/ri/openjdk-11+28_windows-x64_bin.zip
eclipse_link=http://mirror.csclub.uwaterloo.ca/eclipse/eclipse/downloads/drops4/R-4.11-201903070500/eclipse-platform-4.11-win32-x86_64.zip
cmder_link=https://github.com/cmderdev/cmder/releases/download/v1.3.11/cmder.zip
python_link=https://github.com/winpython/winpython/releases/download/1.10.20180404/WinPython64-3.6.5.0Zero.exe
svn_link=https://sourceforge.net/projects/win32svn/files/1.8.17/apache22/svn-win32-1.8.17.zip
cmake_link=https://cmake.org/files/v3.14/cmake-3.14.4-win64-x64.zip
ninja_link=https://github.com/ninja-build/ninja/releases/download/v1.9.0/ninja-win.zip
qt5_msvc_link=https://download.qt.io/archive/qt/5.8/5.8.0/qt-opensource-windows-x86-msvc2013-5.8.0.exe
qt5_mingw_link=https://download.qt.io/archive/qt/5.8/5.8.0/qt-opensource-windows-x86-mingw530-5.8.0.exe
freeglut_msvc_link=http://files.transmissionzero.co.uk/software/development/GLUT/freeglut-MSVC.zip
freeglut_mingw_link=http://files.transmissionzero.co.uk/software/development/GLUT/freeglut-MinGW.zip
glew_msvc_link=https://sourceforge.net/projects/glew/files/glew/2.1.0/glew-2.1.0-win32.zip
glew_mingw_link=https://sourceforge.net/projects/glew/files/glew/2.1.0/glew-2.1.0.zip
doxygen_link=http://doxygen.nl/files/doxygen-1.8.15.windows.x64.bin.zip
vscode_link=https://go.microsoft.com/fwlink/?Linkid=850641
thunderbird_link=https://ftp.mozilla.org/pub/thunderbird/nightly/latest-comm-central/thunderbird-69.0a1.en-US.win64.zip
firefox_link=http://ftp.mozilla.org/pub/firefox/candidates/67.0-candidates/build2/win64/en-US/firefox-67.0.zip
npp_link=https://notepad-plus-plus.org/repository/7.x/7.6.6/npp.7.6.6.bin.x64.zip
imagemagick_link=https://imagemagick.org/download/binaries/ImageMagick-7.0.8-49-portable-Q16-x64.zip

jdk_file=$(basename $jdk_link)
eclipse_file=$(basename $eclipse_link)
cmder_file=$(basename $cmder_link)
python_file=$(basename $python_link)
svn_file=$(basename $svn_link)
cmake_file=$(basename $cmake_link)
ninja_file=$(basename $ninja_link)
qt5_mingw_file=$(basename $qt5_mingw_link)
qt5_msvc_file=$(basename $qt5_msvc_link)
freeglut_msvc_file=$(basename $freeglut_msvc_link)
freeglut_mingw_file=$(basename $freeglut_mingw_link)
glew_msvc_file=$(basename $glew_msvc_link)
glew_mingw_file=$(basename $glew_mingw_link)
doxygen_file=$(basename $doxygen_link)
vscode_file=vscode.zip
thunderbird_file=$(basename $thunderbird_link)
firefox_file=$(basename $firefox_link)
npp_file=$(basename $npp_link)
imagemagick_file=$(basename $imagemagick_link)

wnix_path=$(dirname $(realpath $0))
download_path=$wnix_path/download
extract_path=$wnix_path

download_check=$wnix_path/.download
extract_check=$wnix_path/.extract
provision_check=$wnix_path/.provision

PATH=$PATH:/mingw64/bin

download() {
    mkdir -p $download_path
    mkdir -p $download_path/mingw
    mkdir -p $download_path/msvc

    if [ ! -f $download_path/$jdk_file ]; then
        echo "Downloading jre ..."
        curl -L -o $download_path/$jdk_file -b "oraclelicense=a" -k $jdk_link
    fi
    
    if [ ! -f $download_path/$eclipse_file ]; then
        echo "Downloading eclipse ..."
        curl -L -o $download_path/$eclipse_file -k $eclipse_link
    fi
    
    if [ ! -f $download_path/$cmder_file ]; then
        echo "Downloading cmder ..."
        curl -L -o $download_path/$cmder_file -k $cmder_link
    fi
        
    if [ ! -f $download_path/$python_file ]; then
        echo "Downloading python ..."
        curl -L -o $download_path/$python_file -k $python_link
    fi

    if [ ! -f $download_path/$svn_file ]; then
        echo "Downloading svn cli ..."
        curl -L -o $download_path/$svn_file -k $svn_link
    fi

    if [ ! -f $download_path/$cmake_file ]; then
        echo "Downloading cmake ..."
        curl -L -o $download_path/$cmake_file -k $cmake_link
    fi

    if [ ! -f $download_path/$ninja_file ]; then
        echo "Downloading ninja ..."
        curl -L -o $download_path/$ninja_file -k $ninja_link
    fi

    if [ ! -f $download_path/$qt5_msvc_file ]; then
        echo "Downloading qt5 msvc..."
        curl -L -o $download_path/$qt5_msvc_file -k $qt5_msvc_link
    fi

    if [ ! -f $download_path/$qt5_mingw_file ]; then
        echo "Downloading qt5 mingw..."
        curl -L -o $download_path/$qt5_mingw_file -k $qt5_mingw_link
    fi

    if [ ! -f $download_path/$freeglut_msvc_file ]; then
        echo "Downloading freeglut msvc ..."
        curl -L -o $download_path/$freeglut_msvc_file -k $freeglut_msvc_link
    fi

    if [ ! -f $download_path/$freeglut_mingw_file ]; then
        echo "Downloading freeglut mingw ..."
        curl -L -o $download_path/$freeglut_mingw_file -k $freeglut_mingw_link
    fi

    if [ ! -f $download_path/$glew_msvc_file ]; then
        echo "Downloading glew msvc ..."
        curl -L -o $download_path/$glew_msvc_file -k $glew_msvc_link
    fi

    if [ ! -f $download_path/$glew_mingw_file ]; then
        echo "Downloading glew mingw ..."
        curl -L -o $download_path/$glew_mingw_file -k $glew_mingw_link
    fi

    if [ ! -f $download_path/$doxygen_file ]; then
        echo "Downloading doxygen ..."
        curl -L -o $download_path/$doxygen_file -k $doxygen_link
    fi

    if [ ! -f $download_path/$vscode_file ]; then
        echo "Downloading vscode ..."
        curl -L -o $download_path/$vscode_file -k $vscode_link
    fi

    if [ ! -f $download_path/$thunderbird_file ]; then
        echo "Downloading thunderbird ..."
        curl -L -o $download_path/$thunderbird_file -k $thunderbird_link
    fi

    if [ ! -f $download_path/$firefox_file ]; then
        echo "Downloading firefox ..."
        curl -L -o $download_path/$firefox_file -k $firefox_link
    fi

    if [ ! -f $download_path/$npp_file ]; then
        echo "Downloading notepad++ ..."
        curl -L -o $download_path/$npp_file -k $npp_link
    fi

    if [ ! -f $download_path/$imagemagick_file ]; then
        echo "Downloading imagemagick ..."
        curl -L -o $download_path/$imagemagick_file -k $imagemagick_link
    fi

    touch $download_check
}

extract() {
    mkdir -p $extract_path
    mkdir -p $extract_path/mingw
    mkdir -p $extract_path/msvc

    # jdk
    if [ ! -d $extract_path/jdk ]; then
        unzip $download_path/$jdk_file -d $extract_path
        mv $extract_path/jdk* $extract_path/jdk
    fi
    
    # eclipse
    if [ ! -d $extract_path/eclipse ]; then
        unzip $download_path/$eclipse_file -d $extract_path
    fi
    
    # cmder
    if [ ! -d $extract_path/cmder ]; then
	mkdir -p $extract_path/cmder
        unzip $download_path/$cmder_file -d $extract_path/cmder
    fi
    
    # python
    if [ ! -d $download_path/winpython3 ]; then
        $download_path/$python_file /S /D=$(cygpath -w $download_path/winpython3)
    fi

    # svn
    if [ ! -d $extract_path/svn ]; then
        unzip $download_path/$svn_file -d $extract_path
        mv $extract_path/svn* $extract_path/svn
    fi

    # cmake 
    if [ ! -d $extract_path/cmake ]; then
        unzip $download_path/$cmake_file -d $extract_path
        mv $extract_path/cmake* $extract_path/cmake
    fi

    # ninja
    if [ ! -x $extract_path/ninja ]; then
        echo "Unzipping ninja"
        mkdir -p $extract_path/ninja
        unzip $download_path/$ninja_file -d $extract_path/ninja
    fi

    # qt5 msvc
    if [ ! -d $extract_path/msvc/qt5 ]; then
        $download_path/$qt5_msvc_file --script $wnix_path/provision/qt-unattended-install_msvc.qs InstallDir=$extract_path/msvc/qt5
    fi

    # qt5 mingw
    if [ ! -d $extract_path/mingw/qt5 ]; then
        $download_path/$qt5_mingw_file --script $wnix_path/provision/qt-unattended-install_mingw.qs InstallDir=$extract_path/mingw/qt5
    fi

    # freeglut msvc
    if [ ! -d $extract_path/msvc/freeglut ]; then
        unzip $download_path/$freeglut_msvc_file -d $extract_path/msvc
    fi

    # freeglut mingw
    if [ ! -d $extract_path/mingw/freeglut ]; then
        unzip $download_path/$freeglut_mingw_file -d $extract_path/mingw
    fi

    # glew msvc
    if [ ! -d $extract_path/msvc/glew ]; then
        unzip -o $download_path/$glew_msvc_file -d $extract_path/msvc
    fi

    # glew mingw
    if [ ! -d $extract_path/mingw/glew ]; then
        unzip -o $download_path/$glew_mingw_file -d $download_path/mingw
        mv $download_path/mingw/glew* $extract_path/mingw/glew_src
    fi

    # doxygen
    if [ ! -d $extract_path/doxygen ]; then
        mkdir -p $extract_path/doxygen
        unzip $download_path/$doxygen_file -d $extract_path/doxygen
    fi

    # vscode
    if [ ! -d $extract_path/vscode ]; then
        mkdir -p $extract_path/vscode
        unzip $download_path/$vscode_file -d $extract_path/vscode
    fi

    # thunderbird
    if [ ! -d $extract_path/thunderbird ]; then
        unzip $download_path/$thunderbird_file -d $extract_path
    fi

    # firefox
    if [ ! -d $extract_path/firefox ]; then
        unzip $download_path/$firefox_file -d $extract_path
    fi

    # notepad++
    if [ ! -d $extract_path/npp ]; then
        mkdir -p $extract_path/npp
        unzip $download_path/$npp_file -d $extract_path/npp
    fi

    # imagemagick
    if [ ! -d $extract_path/imagemagick ]; then
        mkdir -p $extract_path/imagemagick
        unzip $download_path/$imagemagick_file -d $extract_path/imagemagick
    fi

    touch $extract_check
}

provision() {
    echo "Changing Cmder defaults"
    cp $wnix_path/provision/vimrc $extract_path/cmder/vendor/git-for-windows/etc/vimrc
    cp $wnix_path/provision/ConEmu.xml $extract_path/cmder/vendor/conemu-maximus5/ConEmu.xml
    cp $wnix_path/provision/user_* $extract_path/cmder/config/
    
    if [ ! -d $extract_path/python3 ]; then
        echo "Taking only python3 distribution from WinPython"
        mkdir -p $extract_path/python3
        cp -R $download_path/winpython3/python-3.*/* $extract_path/python3/
    fi

    PATH=$extract_path/python3:$PATH
    PATH=$extract_path/python3/Scripts:$PATH
    pip install pyqt5 pyqtchart pyinstaller jinja2 numpy Pillow matplotlib

    # Manually download wheel files from https://www.lfd.uci.edu/~gohlke/pythonlibs
    # and put them into download directory as we can't get full download link.
    pip install download/GDAL-3.0.0-cp36-cp36m-win_amd64.whl
    pip install download/rasterio-1.0.23-cp36-cp36m-win_amd64.whl

    if [ ! -d $extract_path/mingw/glew ]; then
        echo "Building glew for mingw"
        PATH=$extract_path/mingw/qt5/Tools/mingw530_32/bin:$PATH
        pushd .
        cd $extract_path/mingw/glew_src
        mingw32-make clean
        mingw32-make glew.lib
        mingw32-make bin
        popd
        mv $extract_path/mingw/glew_src/lib/*.dll $extract_path/mingw/glew_src/bin/
        mkdir -p $extract_path/mingw/glew
        cp -a $extract_path/mingw/glew_src/include $extract_path/mingw/glew/
        cp -a $extract_path/mingw/glew_src/lib $extract_path/mingw/glew/
        cp -a $extract_path/mingw/glew_src/bin $extract_path/mingw/glew/
        rm -rf $extract_path/mingw/glew_src
    fi

    if [ ! -d $extract_path/msvc/glew ]; then
        echo "Making glew msvc folder structure saner"
        mv $extract_path/msvc/glew* $extract_path/msvc/glew_tmp
        mkdir -p $extract_path/msvc/glew/bin
        mkdir -p $extract_path/msvc/glew/lib
        mkdir -p $extract_path/msvc/glew/include
        mv $extract_path/msvc/glew_tmp/bin/Release/Win32/* $extract_path/msvc/glew/bin
        mv $extract_path/msvc/glew_tmp/lib/Release/Win32/* $extract_path/msvc/glew/lib
        mv $extract_path/msvc/glew_tmp/include/* $extract_path/msvc/glew/include
        rm -rf $extract_path/msvc/glew_tmp

    fi

    echo "Copying mingw32-make to make"
    cp $extract_path/mingw/qt5/Tools/mingw530_32/bin/mingw32-make.exe \
    $extract_path/mingw/qt5/Tools/mingw530_32/bin/make.exe 

    echo "Deleting extra components from qt msvc"
    mv $extract_path/msvc/qt5/5.8 $extract_path/msvc
    rm -rf $extract_path/msvc/qt5/*
    mv $extract_path/msvc/5.8 $extract_path/msvc/qt5/

    echo "Installing vscode plugins"
    data_dir=$extract_path/vscode/data
    mkdir -p $data_dir/tmp
    cpptools_link=https://github.com/microsoft/vscode-cpptools/releases/download/0.23.1/cpptools-win32.vsix
    curl -L -o $data_dir/tmp/cpptools-win32.vsix -k $cpptools_link
    PATH=$extract_path/vscode/bin:$PATH
    code --install-extension $data_dir/tmp/cpptools-win32.vsix \
         --install-extension vector-of-bool.cmake-tools \
         --install-extension maddouri.cmake-tools-helper \
         --install-extension twxs.cmake \
         --install-extension ms-python.python \
         --install-extension yzhang.markdown-all-in-one \
         --install-extension vscodevim.vim \
         --install-extension EditorConfig.editorconfig \
         --install-extension johnstoncode.svn-scm \
         --install-extension donjayamanne.githistory \
         --install-extension eamodio.gitlens
    rm -rf $data_dir/tmp

    echo "Installing eclipse plugins"
    # eclipse
    repoUrls=http://download.eclipse.org/releases/2019-03
    repoUrls=$repoUrls,http://download.eclipse.org/eclipse/updates/4.11
    
    # c/c++
    repoUrls=$repoUrls,http://download.eclipse.org/tools/cdt/releases/9.7
    features=org.eclipse.cdt.feature.group
    
    # terminal
    repoUrls=$repoUrls,http://download.eclipse.org/tm/terminal/marketplace
    features=$features,org.eclipse.tm.terminal.feature.feature.group
    
    # pydev
    repoUrls=$repoUrls,http://pydev.org/updates
    features=$features,org.python.pydev.feature.feature.group
    
    # vrapper
    repoUrls=$repoUrls,http://vrapper.sourceforge.net/update-site/stable
    features=$features,net.sourceforge.vrapper.feature.group
    features=$features,net.sourceforge.vrapper.plugin.splitEditor.feature.group

    # perl
    repoUrls=$repoUrls,http://www.epic-ide.org/updates/testing
    features=$features,org.epic.feature.main.feature.group

    # java
    features=$features,org.eclipse.jdt.feature.group
    
    # svn
    repoUrls=$repoUrls,https://dl.bintray.com/subclipse/releases/subclipse/latest/
    features=$features,org.tmatesoft.svnkit.feature.group
    features=$features,org.tigris.subversion.clientadapter.javahl.feature.feature.group
    features=$features,org.tigris.subversion.subclipse.feature.group
    features=$features,org.tigris.subversion.subclipse.graph.feature.feature.group

    # dxl editor
    repoUrls=$repoUrls,http://download.sodius.com/
    features=$features,com.sodius.mdw.platform.doors.dxl.rcp.feature.group

    # cmake
    repoUrls=$repoUrls,https://raw.githubusercontent.com/15knots/cmake4eclipse/master/releng/comp-update/
    features=$features,de.marw.cdt.cmake.feature.group
    repoUrls=$repoUrls,https://raw.githubusercontent.com/15knots/cmakeed/master/cmakeed-update/
    features=$features,com.cthing.cmakeed.feature.feature.group

    # xml
    features=$features,org.eclipse.wst.xml_ui.feature.feature.group

    # git
    features=$features,org.eclipse.egit.feature.group

    # emf
    features=$features,org.eclipse.emf.sdk.feature.group
    features=$features,org.eclipse.emf.ecoretools.sdk.feature.group

    # ocl
    features=$features,org.eclipse.ocl.all.sdk.feature.group
    features=$features,org.eclipse.ocl.examples.feature.group

    # Visual C++ Support
    features=$features,org.eclipse.cdt.msw.feature.group

    PATH=$extract_path/jdk/bin:$PATH
    $extract_path/eclipse/eclipsec.exe \
    -clean \
    -purgeHistory \
    -application org.eclipse.equinox.p2.director \
    -noSplash \
    -repository $repoUrls \
    -installIUs $features \
    -destination $extract_path/eclipse \
    -profile SDKProfile

    # Copy firefox.exe to ffirefox.exe
    cp $extract_path/firefox/firefox.exe $extract_path/firefox/ffirefox.exe

    touch $provision_check
}

release() {
    tar  zcvf ../wnix-$1.tar.gz --exclude="download"    \
                                --exclude=".download"   \
                                --exclude=".extract"    \
                                --exclude=".provision"  \
                                --exclude=".git" .
}

usage() {
    echo "Usage: `basename $0` [options]"
    echo
    echo "Options"
    echo "   -h, --help           Prints this help message"
    echo "   -d, --download       Downloads programs"
    echo "   -x, --extract        Extracts downloaded programs"
    echo "   -p, --provision      Provisions extracted programs"
    echo "   -r, --release        Releases a tarball with extracted programs"
}

main() {
    if [ $# -eq 0 ]; then
        echo "No option given"
        usage
        exit 1
    fi

    while [ $# -gt 0 ]; do
        key=$1
        case $key in
            -d|--download)
                will_download=yes
                ;;
            -x|--extract)
                will_extract=yes
                ;;
            -p|--provision)
                will_provision=yes
                ;;
            -r|--release)
                will_release=yes
                ;;
            -h|--help)
                usage
                exit 0
                ;;
            *)
                echo "Unknown option: $key"
                usage
                exit 1
                ;;
        esac
        shift
    done

    if [ ! -z $will_download ]; then
        download
        echo "Download done!"
    fi

    if [ ! -z $will_extract ]; then
        if [ -f $download_check ]; then
            extract
            echo "Extraction done!"
        else
            echo "Nothing to extract. You should first download!"
            usage
            exit 1
        fi
    fi
    
    if [ ! -z $will_provision ]; then
        if [ -f $extract_check ]; then
            provision
            echo "Provision done!"
        else
            echo "Nothing to provision. You should first extract the programs!"
            usage
            exit 1
        fi
    fi

    if [ ! -z $will_release ]; then
        if [ -f $provision_check ]; then
            release $wnix_version
            echo "Release done!"
        else
            echo "Cannot release. You should first provision the programs!"
            usage
            exit 1
        fi
    fi
}

main $@
