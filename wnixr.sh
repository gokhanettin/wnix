#!/bin/sh

set -e

wnix_version="v0.1.12"

jre_link=http://download.oracle.com/otn-pub/java/jdk/8u172-b11/a58eab1ec242421181065cdc37240b08/jre-8u172-windows-x64.tar.gz
eclipse_link=http://mirror.csclub.uwaterloo.ca/eclipse/eclipse/downloads/drops4/R-4.7.3a-201803300640/eclipse-platform-4.7.3a-win32-x86_64.zip
cmder_link=https://github.com/cmderdev/cmder/releases/download/v1.3.5/cmder.7z
python_link=https://github.com/winpython/winpython/releases/download/1.10.20180404/WinPython64-3.6.5.0Zero.exe
svn_link=https://sourceforge.net/projects/win32svn/files/1.8.17/apache22/svn-win32-1.8.17.zip
cmake_link=https://cmake.org/files/v3.11/cmake-3.11.1-win64-x64.zip
ninja_link=https://github.com/ninja-build/ninja/releases/download/v1.8.2/ninja-win.zip
qt5_link=https://download.qt.io/official_releases/qt/5.9/5.9.1/qt-opensource-windows-x86-5.9.1.exe
freeglut_link=http://files.transmissionzero.co.uk/software/development/GLUT/freeglut-MinGW.zip
glew_link=https://sourceforge.net/projects/glew/files/glew/2.1.0/glew-2.1.0.zip
doxygen_link=http://ftp.stack.nl/pub/users/dimitri/doxygen-1.8.14.windows.x64.bin.zip
vscode_link=https://code.visualstudio.com/docs/?dv=winzip
thunderbird_link=https://ftp.mozilla.org/pub/thunderbird/nightly/latest-comm-central/thunderbird-62.0a1.en-US.win64.zip
firefox_link=http://ftp.mozilla.org/pub/firefox/candidates/60.0-candidates/build2/win64/en-US/firefox-60.0.zip

jre_file=$(basename $jre_link)
eclipse_file=$(basename $eclipse_link)
cmder_file=$(basename $cmder_link)
python_file=$(basename $python_link)
svn_file=$(basename $svn_link)
cmake_file=$(basename $cmake_link)
ninja_file=$(basename $ninja_link)
qt5_file=$(basename $qt5_link)
freeglut_file=$(basename $freeglut_link)
glew_file=$(basename $glew_link)
doxygen_file=$(basename $doxygen_link)
vscode_file=vscode.zip
thunderbird_file=$(basename $thunderbird_link)
firefox_file=$(basename $firefox_link)

wnix_path=$(dirname $(realpath $0))
download_path=$wnix_path/download
extract_path=$wnix_path

download_check=$wnix_path/.download
extract_check=$wnix_path/.extract
provision_check=$wnix_path/.provision

PATH=$PATH:/mingw64/bin

download() {
    mkdir -p $download_path

    if [ ! -f $download_path/$jre_file ]; then
        echo "Downloading jre ..."
        curl -L -o $download_path/$jre_file -H "Cookie: oraclelicense=accept-securebackup-cookie" -k $jre_link
    fi
    
    if [ ! -f $download_path/$eclipse_file ]; then
        echo "Downloading eclipse ..."
        curl -L -o $download_path/$eclipse_file -k $eclipse_link
    fi
    
    if [ ! -f $download_path/$cmder_file ]; then
        echo "Downloading cmder ..."
        curl -L -o $download_path/$cmder_file -k $cmder_link
    fi
        
    if [ ! -f $download_path/$python_file ]; then
        echo "Downloading python ..."
        curl -L -o $download_path/$python_file -k $python_link
    fi

    if [ ! -f $download_path/$svn_file ]; then
        echo "Downloading svn cli ..."
        curl -L -o $download_path/$svn_file -k $svn_link
    fi

    if [ ! -f $download_path/$cmake_file ]; then
        echo "Downloading cmake ..."
        curl -L -o $download_path/$cmake_file -k $cmake_link
    fi

    if [ ! -f $download_path/$ninja_file ]; then
        echo "Downloading ninja ..."
        curl -L -o $download_path/$ninja_file -k $ninja_link
    fi

    if [ ! -f $download_path/$qt5_file ]; then
        echo "Downloading qt5 ..."
        curl -L -o $download_path/$qt5_file -k $qt5_link
    fi

    if [ ! -f $download_path/$freeglut_file ]; then
        echo "Downloading freeglut ..."
        curl -L -o $download_path/$freeglut_file -k $freeglut_link
    fi

    if [ ! -f $download_path/$glew_file ]; then
        echo "Downloading glew ..."
        curl -L -o $download_path/$glew_file -k $glew_link
    fi

    if [ ! -f $download_path/$doxygen_file ]; then
        echo "Downloading doxygen ..."
        curl -L -o $download_path/$doxygen_file -k $doxygen_link
    fi

    if [ ! -f $download_path/$vscode_file ]; then
        echo "Downloading vscode ..."
        curl -L -o $download_path/$vscode_file -k $vscode_link
    fi

    if [ ! -f $download_path/$thunderbird_file ]; then
        echo "Downloading thunderbird ..."
        curl -L -o $download_path/$thunderbird_file -k $thunderbird_link
    fi

    if [ ! -f $download_path/$firefox_file ]; then
        echo "Downloading firefox ..."
        curl -L -o $download_path/$firefox_file -k $firefox_link
    fi

    touch $download_check
}

extract() {
    mkdir -p $extract_path

    # jre
    if [ ! -d $extract_path/jre ]; then
        tar -xzvf $download_path/$jre_file -C $extract_path
        mv $extract_path/jre* $extract_path/jre
    fi
    
    # eclipse
    if [ ! -d $extract_path/eclipse ]; then
        unzip $download_path/$eclipse_file -d $extract_path
    fi
    
    # cmder
    if [ ! -d $extract_path/cmder ]; then
        mkdir -p $extract_path/cmder
        "/c/Program Files/7-Zip/7z" x $download_path/$cmder_file -o$extract_path/cmder
    fi
    
    # python
    if [ ! -d $download_path/winpython3 ]; then
        $download_path/$python_file //S /D=$(cygpath -w $download_path/winpython3)
    fi

    # svn
    if [ ! -d $extract_path/svn ]; then
        unzip $download_path/$svn_file -d $extract_path
        mv $extract_path/svn* $extract_path/svn
    fi

    # cmake 
    if [ ! -d $extract_path/cmake ]; then
        unzip $download_path/$cmake_file -d $extract_path
        mv $extract_path/cmake* $extract_path/cmake
    fi

    # qt5
    if [ ! -d $extract_path/qt5 ]; then
        $download_path/$qt5_file --script $wnix_path/provision/qt-unattended-install.qs InstallDir=$extract_path/qt5
    fi

    # freeglut
    if [ ! -d $extract_path/freeglut ]; then
        unzip $download_path/$freeglut_file -d $extract_path
    fi

    # glew
    if [ ! -d $extract_path/glew_src ]; then
        unzip -o $download_path/$glew_file -d $download_path
        mv $download_path/$glew_file $download_path/_$glew_file
        mv $download_path/glew* $extract_path/glew_src
        mv $download_path/_$glew_file $download_path/$glew_file
    fi

    # doxygen
    if [ ! -d $extract_path/doxygen ]; then
        mkdir -p $extract_path/doxygen
        unzip $download_path/$doxygen_file -d $extract_path/doxygen
    fi

    # vscode
    if [ ! -d $extract_path/vscode ]; then
        mkdir -p $extract_path/vscode
        unzip $download_path/$vscode_file -d $extract_path/vscode
    fi

    # thunderbird
    if [ ! -d $extract_path/thunderbird ]; then
        unzip $download_path/$thunderbird_file -d $extract_path
    fi

    # firefox
    if [ ! -d $extract_path/firefox ]; then
        unzip $download_path/$firefox_file -d $extract_path
    fi

    touch $extract_check
}

provision() {
    if [ ! -x $extract_path/extra/bin/ninja ]; then
        echo "Unzipping ninja"
        mkdir -p $extract_path/extra/bin
        unzip $download_path/$ninja_file -d $extract_path/extra/bin
    fi

    echo "Changing Cmder defaults"
    cp $wnix_path/provision/vimrc $extract_path/cmder/vendor/git-for-windows/etc/vimrc
    cp $wnix_path/provision/ConEmu.xml $extract_path/cmder/vendor/conemu-maximus5/ConEmu.xml
    cp $wnix_path/provision/init.bat $extract_path/cmder/vendor/init.bat
    
    if [ ! -d $extract_path/python3 ]; then
        echo "Taking only python3 distribution from WinPython"
        mkdir -p $extract_path/python3
        cp -R $download_path/winpython3/python-3.*/* $extract_path/python3/
        echo "Installing PyQt5..."
        PATH=$extract_path/python3:$PATH
        $extract_path/python3/Scripts/pip install pyqt5

    fi

    echo "Building glew"
    PATH=$extract_path/qt5/Tools/mingw530_32/bin:$PATH
    pushd .
    cd $extract_path/glew_src
    mingw32-make clean
    mingw32-make glew.lib
    mingw32-make bin
    popd
    mv $extract_path/glew_src/lib/*.dll $extract_path/glew_src/bin/
    mkdir -p $extract_path/glew
    cp -a $extract_path/glew_src/include $extract_path/glew/
    cp -a $extract_path/glew_src/lib $extract_path/glew/
    cp -a $extract_path/glew_src/bin $extract_path/glew/
    rm -rf $extract_path/glew_src

    echo "Copying mingw32-make to make"
    cp $extract_path/qt5/Tools/mingw530_32/bin/mingw32-make.exe \
    $extract_path/qt5/Tools/mingw530_32/bin/make.exe 

    echo "Installing vscode plugins"
    extensions_dir=$extract_path/vscode/extensions
    mkdir -p $extensions_dir
    PATH=$extract_path/vscode/bin:$PATH
    code --install-extension ms-vscode.cpptools \
         --install-extension vector-of-bool.cmake-tools \
         --install-extension maddouri.cmake-tools-helper \
         --install-extension  twxs.cmake \
         --install-extension ms-python.python \
         --install-extension yzhang.markdown-all-in-one \
         --install-extension vscodevim.vim \
         --install-extension EditorConfig.editorconfig \
         --install-extension johnstoncode.svn-scm \
         --install-extension  ms-vscode.powershell \
         --extensions-dir $extensions_dir

    echo "Installing eclipse plugins"
    # eclipse
    repoUrls=http://download.eclipse.org/eclipse/updates/4.7
    repoUrls=$repoUrls,http://download.eclipse.org/releases/oxygen
    
    # c/c++
    repoUrls=$repoUrls,http://download.eclipse.org/tools/cdt/releases/9.4
    features=org.eclipse.cdt.feature.group
    
    # terminal
    repoUrls=$repoUrls,http://download.eclipse.org/tm/terminal/marketplace
    features=$features,org.eclipse.tm.terminal.feature.feature.group
    
    # pydev
    repoUrls=$repoUrls,http://pydev.org/updates
    features=$features,org.python.pydev.feature.feature.group
    
    # vrapper
    repoUrls=$repoUrls,http://vrapper.sourceforge.net/update-site/stable
    features=$features,net.sourceforge.vrapper.feature.group
    features=$features,net.sourceforge.vrapper.plugin.splitEditor.feature.group

    # perl
    repoUrls=$repoUrls,http://www.epic-ide.org/updates/testing
    features=$features,org.epic.feature.main.feature.group
    
    # java - groovy plugin requires this
    features=$features,org.eclipse.jdt.feature.group
    
    # groovy
    repoUrls=$repoUrls,http://dist.springsource.org/snapshot/GRECLIPSE/e4.7/
    features=$features,org.codehaus.groovy.eclipse.feature.feature.group
    
    # svn
    repoUrls=$repoUrls,https://dl.bintray.com/subclipse/releases/subclipse/latest/
    features=$features,org.tmatesoft.svnkit.feature.group
    features=$features,org.tigris.subversion.clientadapter.javahl.feature.feature.group
    features=$features,org.tigris.subversion.subclipse.feature.group
    features=$features,org.tigris.subversion.subclipse.graph.feature.feature.group

    # dxl editor
    repoUrls=$repoUrls,http://download.sodius.com/
    features=$features,com.sodius.mdw.platform.doors.dxl.rcp.feature.group

    # cmake
    repoUrls=$repoUrls,https://raw.githubusercontent.com/15knots/cmake4eclipse/master/releng/comp-update/
    features=$features,de.marw.cdt.cmake.feature.group
    repoUrls=$repoUrls,https://raw.githubusercontent.com/15knots/cmakeed/master/cmakeed-update/
    features=$features,com.cthing.cmakeed.feature.feature.group

    # markdown
    repoUrls=$repoUrls,http://www.nodeclipse.org/updates/markdown/
    features=$features,markdown.editor.feature.feature.group

    cp $wnix_path/provision/eclipse.ini $extract_path/eclipse/eclipse.ini
    PATH=$extract_path/jre/bin:$PATH
    $extract_path/eclipse/eclipsec.exe \
    -clean \
    -purgeHistory \
    -application org.eclipse.equinox.p2.director \
    -noSplash \
    -repository $repoUrls \
    -installIUs $features \
    -destination $extract_path/eclipse \
    -profile SDKProfile

    touch $provision_check
}

release() {
    tar jcvf ../wnix-$1.tar.bz2 --exclude="download"    \
                                --exclude=".download"   \
                                --exclude=".extract"    \
                                --exclude=".provision"  \
                                --exclude-vcs .
}

usage() {
    echo "Usage: `basename $0` [options]"
    echo
    echo "Options"
    echo "   -h, --help           Prints this help message"
    echo "   -d, --download       Downloads programs"
    echo "   -x, --extract        Extracts downloaded programs"
    echo "   -p, --provision      Provisions extracted programs"
    echo "   -r, --release        Releases a tarball with extracted programs"
}

main() {
    if [ $# -eq 0 ]; then
        echo "No option given"
        usage
        exit 1
    fi

    while [ $# -gt 0 ]; do
        key=$1
        case $key in
            -d|--download)
                will_download=yes
                ;;
            -x|--extract)
                will_extract=yes
                ;;
            -p|--provision)
                will_provision=yes
                ;;
            -r|--release)
                will_release=yes
                ;;
            -h|--help)
                usage
                exit 0
                ;;
            *)
                echo "Unknown option: $key"
                usage
                exit 1
                ;;
        esac
        shift
    done

    if [ ! -z $will_download ]; then
        download
        echo "Download done!"
    fi

    if [ ! -z $will_extract ]; then
        if [ -f $download_check ]; then
            extract
            echo "Extraction done!"
        else
            echo "Nothing to extract. You should first download!"
            usage
            exit 1
        fi
    fi
    
    if [ ! -z $will_provision ]; then
        if [ -f $extract_check ]; then
            provision
            echo "Provision done!"
        else
            echo "Nothing to provision. You should first extract the programs!"
            usage
            exit 1
        fi
    fi

    if [ ! -z $will_release ]; then
        if [ -f $provision_check ]; then
            release $wnix_version
            echo "Release done!"
        else
            echo "Cannot release. You should first provision the programs!"
            usage
            exit 1
        fi
    fi
}

main $@
